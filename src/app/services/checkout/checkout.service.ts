import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Cart } from '../../+store/menu.state';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  private serviceMock: Subject<boolean>;

  constructor() { }

  public checkoutCart(cart: Cart, success = true): Observable<boolean> {
    this.serviceMock = new Subject<boolean>();
    setTimeout(() => {
      if (success) {
        this.serviceMock.next(success);
      } else {
        this.serviceMock.error(success);
      }
    }, 3000);
    return this.serviceMock;
  }
}
