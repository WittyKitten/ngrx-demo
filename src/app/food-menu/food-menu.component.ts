import { Component, OnInit } from '@angular/core';
import { MenuActions } from '../+store/menu.actions';
import { Store } from '@ngrx/store';
import { MenuState } from '../+store/menu.state';

@Component({
  selector: 'demo-food-menu',
  templateUrl: './food-menu.component.html',
  styleUrls: ['./food-menu.component.scss']
})
export class FoodMenuComponent implements OnInit {

  constructor(private store$: Store<MenuState>) { }

  ngOnInit(): void {
  }

  addBurger(amount) {
    this.store$.dispatch(MenuActions.AddBurger({amount: parseInt(amount)}));
  }
  addPizza() {
    this.store$.dispatch(MenuActions.AddPizza());
  }
  addSushi() {
    this.store$.dispatch(MenuActions.AddSushi());
  }
  addSlapChips() {
    this.store$.dispatch(MenuActions.AddSlapChips());
  }
}
