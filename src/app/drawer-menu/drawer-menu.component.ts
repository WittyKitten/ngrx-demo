import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { MenuState } from '../+store/menu.state';
import {
  selectBurgersInCart,
  selectPizzaInCart,
  selectSlaChipsInCart,
  selectSushiInCart
} from '../+store/menu.selectors';

@Component({
  selector: 'demo-drawer-menu',
  templateUrl: './drawer-menu.component.html',
  styleUrls: ['./drawer-menu.component.scss']
})
export class DrawerMenuComponent implements OnInit {
  burgerCount$: Observable<number>;
  pizzaCount$: Observable<number>;
  sushiCount$: Observable<number>;
  slapChipsCount$: Observable<number>;

  constructor(private store$: Store<MenuState>) { }

  ngOnInit(): void {
    this.burgerCount$ = this.store$.select(selectBurgersInCart);
    this.pizzaCount$ = this.store$.select(selectPizzaInCart);
    this.sushiCount$ = this.store$.select(selectSushiInCart);
    this.slapChipsCount$ = this.store$.select(selectSlaChipsInCart);
  }

}
