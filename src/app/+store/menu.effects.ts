import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MenuActions } from './menu.actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CheckoutService } from '../services/checkout/checkout.service';
import { Store } from '@ngrx/store';
import { MenuState } from './menu.state';
import { selectCart } from './menu.selectors';
import { of } from 'rxjs';

@Injectable()
export class MenuEffects {
    constructor(
        private actions$: Actions,
        private _snackBar: MatSnackBar,
        private checkoutService: CheckoutService,
        private store$: Store<MenuState>
    ){}

    // non dispatching effect
    itemAddedToCart$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MenuActions.AddSlapChips, MenuActions.AddSushi, MenuActions.AddPizza, MenuActions.AddBurger),
            tap((action) => {
                this._snackBar.open('Item added to cart', 'Dismiss', {
                    duration: 2000,
                });
            } )
        ), { dispatch: false }
    );

    // Dispatching effect
    checkoutCart$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MenuActions.CheckoutCart),
            withLatestFrom(this.store$.select(selectCart)),
            switchMap(([action, cart]) => this.checkoutService.checkoutCart(cart, action.success).pipe(
                map((response) => MenuActions.CartCheckoutSuccess()),
                catchError((error) => {
                    return of(MenuActions.CartCheckoutFailure());
                })
            ))
        )
    );

    // non dispatching effect
    cartCheckoutFailure$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MenuActions.CartCheckoutFailure),
            tap((action) => {
                this._snackBar.open('An error occurred when attempting to check out the selected cart', 'Dismiss', {
                    duration: 10000,
                });
            } )
        ), { dispatch: false }
    );

    // non dispatching effect
    cartCheckoutSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MenuActions.CartCheckoutSuccess),
            tap((action) => {
                this._snackBar.open('Cart successfully checked out.', 'Dismiss', {
                    duration: 10000,
                });
            } )
        ), { dispatch: false }
    );
}
