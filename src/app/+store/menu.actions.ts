import { createAction, props } from '@ngrx/store';

const CheckoutCart = createAction(
    '[Top Nav Bar] Checkout Cart',
    props<{ success: boolean }>()
);

const AddBurger = createAction(
    '[Menu Page] Add Burger',
    props<{ amount: number }>()
);

const AddPizza = createAction('[Menu Page] Add Pizza');
const AddSlapChips = createAction('[Menu Page] Add Slap Chips');
const AddSushi = createAction('[Menu Page] Add Sushi');
const CartCheckoutSuccess = createAction('[Top Nav Bar] Checkout Cart Success');
const CartCheckoutFailure = createAction('[Top Nav Bar] Checkout Cart Failure');

export const MenuActions = {
    AddBurger,
    AddPizza,
    AddSlapChips,
    AddSushi,
    CheckoutCart,
    CartCheckoutSuccess,
    CartCheckoutFailure,
};
