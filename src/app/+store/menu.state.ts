export interface Cart {
    burger: number;
    pizza: number;
    slapChips: number;
    sushi: number;
}

export interface MenuState {
    cart: Cart;
    loading: boolean;
}

export const initialState: MenuState = {
    cart: {
        burger: 0,
        pizza: 0,
        slapChips: 0,
        sushi: 0,
    },
    loading: false
}

