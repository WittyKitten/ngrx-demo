import { MenuState } from './menu.state';
import { createSelector } from '@ngrx/store';

export const selectMenuState = (state): MenuState => state?.menu;

export const selectCart = createSelector(selectMenuState, state => state?.cart);
export const selectBurgersInCart = createSelector(selectCart, state => state?.burger | 0);
export const selectPizzaInCart = createSelector(selectCart, state => state?.pizza | 0);
export const selectSushiInCart = createSelector(selectCart, state => state?.sushi | 0);
export const selectSlaChipsInCart = createSelector(selectCart, state => state?.slapChips | 0);

export const selectLoading = createSelector(selectMenuState, state => state.loading);
