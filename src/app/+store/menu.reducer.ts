import { Action, createReducer, on } from '@ngrx/store';
import { initialState, MenuState } from './menu.state';
import { MenuActions } from './menu.actions';

const menuReducer = createReducer(
    initialState,
    on(MenuActions.AddBurger, (state, { amount }): MenuState => {
        return {
            ...state,
            cart: {
                ...state.cart,
                burger: state.cart.burger + amount
            }
        };
    }),
    on(MenuActions.AddPizza, (state => ({...state, cart: {...state.cart, pizza: state.cart.pizza + 1}}))),
    on(MenuActions.AddSlapChips, (state => ({...state, cart: {...state.cart, slapChips: state.cart.slapChips + 1}}))),
    on(MenuActions.AddSushi, (state => ({...state, cart: {...state.cart, sushi: state.cart.sushi + 1}}))),
    on(MenuActions.CartCheckoutSuccess, (() => (initialState))),
    on(MenuActions.CheckoutCart, (state => ({...state, loading: true}))),
    on(MenuActions.CartCheckoutFailure, (state => ({...state, loading: false}))),
);

export function reducer(state: MenuState | undefined, action: Action) {
    return menuReducer(state, action);
}
