import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { MenuState } from './+store/menu.state';
import { MenuActions } from './+store/menu.actions';
import { Observable } from 'rxjs';
import {
  selectBurgersInCart, selectLoading,
  selectPizzaInCart,
  selectSlaChipsInCart,
  selectSushiInCart
} from './+store/menu.selectors';

@Component({
  selector: 'demo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  private burgerCount: number;
  private pizzaCount: number;
  private slapChipsCount: number;
  private sushiCount: number;

  public loading$: Observable<boolean>;

  constructor(private store$: Store<MenuState>) {
  }

  ngOnInit(): void {
    this.loading$ = this.store$.select(selectLoading);
    this.store$.select(selectBurgersInCart).subscribe(amount => this.burgerCount = amount);
    this.store$.select(selectPizzaInCart).subscribe(amount => this.pizzaCount = amount);
    this.store$.select(selectSushiInCart).subscribe(amount => this.sushiCount = amount);
    this.store$.select(selectSlaChipsInCart).subscribe(amount => this.slapChipsCount = amount);
  }


  public get cartTotal(): number {
    return this.burgerCount + this.slapChipsCount + this.sushiCount + this.pizzaCount;
  }

  public checkoutCart(success: boolean) {
    this.store$.dispatch(MenuActions.CheckoutCart({success}));
  }
}
